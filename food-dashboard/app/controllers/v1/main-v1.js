import showInfor, { layThongTinTuForm } from "./controller-v1.js";
import { MonAn } from "../../models/v1/modal-v1.js";
console.log(layThongTinTuForm);

let themMon = () => {
  let data = layThongTinTuForm();
  console.log(data);
  // tao object tu class
  let { ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } = data;
  let monAn = new MonAn(
    ma,
    ten,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  console.log(monAn);
  showInfor(monAn);
};
document.getElementById("btnThemMon").addEventListener("click", themMon);
