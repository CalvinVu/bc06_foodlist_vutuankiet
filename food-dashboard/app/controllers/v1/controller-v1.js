export let layThongTinTuForm = () => {
  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;
  // return {ma:ma, .....} ---> ES6
  return { ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa };
};

let hienThiThongTin = (food) => {
  console.log(food);
};
export default hienThiThongTin;
