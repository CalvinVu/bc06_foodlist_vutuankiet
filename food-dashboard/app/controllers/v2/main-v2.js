import {
  renderFoodList,
  onSuccess,
  showForm,
  layThongTinTuForm,
  Food,
} from "./controller-v2.js";

const BASE_URL = "https://63f442fcfe3b595e2ef03e59.mockapi.io";
// lay du lieu tu sever
let fetchFoodList = () => {
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then(function (res) {
      let classFood = res.data.map((item) => {
        return new Food(
          item.id,
          item.name,
          item.type,
          item.price,
          item.discount,
          item.status,
          item.desc,
          item.img
        );
      });
      console.log(classFood);
      console.log(res);
      renderFoodList(classFood);
    })
    .catch(function (err) {
      console.log(err);
    });
};
fetchFoodList();

//DELETE
let deleteFood = (id) => {
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      fetchFoodList();
      onSuccess("xoá thành công");
    })
    .catch((err) => console.log(err));
};

// USER SU DUNG, KO KHAI BAO HAM SE CHAY BACKEND
window.deleteFood = deleteFood;

// CREATE FOOD
let createFood = () => {
  let data = layThongTinTuForm();
  console.log(data);
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      console.log(res);
      fetchFoodList();
      onSuccess("thêm thành công");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.createFood = createFood;

// EDIT FOOD
let editFood = (id) => {
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      let data = res.data;
      showForm(data);
    })
    .catch((err) => {
      console.log(err);
    });
};
// window.editFood = () => {
//   editFood;
//   $("#exampleModal").modal("show");
// };
window.editFood = editFood;

// UPDATE FOOD
let updateFood = () => {
  let infor = layThongTinTuForm();
  console.log(infor);
  axios({
    url: `${BASE_URL}/food/${infor.id}`,
    method: "PUT",
    data: infor,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      console.log(res);
      fetchFoodList();
      onSuccess("cập nhật thành công");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.updateFood = updateFood;

//  SELECT
let select = () => {
  let choose = document.getElementById("selLoai").value;
  let chay = [];
  let man = [];
  if (choose === "all") {
    axios({
      url: `${BASE_URL}/food`,
      method: "GET",
    })
      .then((res) => {
        let classFood = res.data.map((item) => {
          return new Food(
            item.id,
            item.name,
            item.type,
            item.price,
            item.discount,
            item.status,
            item.desc,
            item.img
          );
        });
        console.log(classFood);
        renderFoodList(classFood);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  if (choose === "Chay") {
    axios({
      url: `${BASE_URL}/food`,
      method: "GET",
    })
      .then((res) => {
        console.log(res);
        let item = res.data.map((item) => {
          return new Food(
            item.id,
            item.name,
            item.type,
            item.price,
            item.discount,
            item.status,
            item.desc,
            item.img
          );
        });
        for (var i = 0; i < item.length; i++) {
          if (item[i].type === "Chay") {
            chay.push(item[i]);
            renderFoodList(chay);
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  if (choose === "Mặn") {
    axios({
      url: `${BASE_URL}/food`,
      method: "GET",
    })
      .then((res) => {
        console.log(res.data);
        let data = res.data.map((item) => {
          return new Food(
            item.id,
            item.name,
            item.type,
            item.price,
            item.discount,
            item.status,
            item.desc,
            item.img
          );
        });
        data.forEach(function (dataMan) {
          if (dataMan.type === "Mặn") {
            console.log(dataMan);
            man.push(dataMan);
            renderFoodList(man);
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
};
window.sele = select;
