export let renderFoodList = (foodArr) => {
  console.log(foodArr);
  let contentHTML = "";
  foodArr.forEach((item) => {
    contentHTML += `
    <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.type}</td>
    <td>${item.price}</td>
    <td>${item.discount}</td>
    <td>${item.giaKhuyenMai()}</td>
    <td>${item.status}</td>
    <td><button class=" px-5 bg-danger"onclick="deleteFood(${
      item.id
    })">Xoá</button><button class="px-5 bg-success" onclick="editFood(${
      item.id
    })" data-toggle="modal" data-target="#exampleModal">Sửa</button></td>
    </tr>`;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

// let isLogin = true;
// isLogin ? "true" : "false";

// TOAST JS : HIEN THI THONG BAO SAU KHI THAO TAC
export let onSuccess = (mess) => {
  Toastify({
    text: mess,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
// LAY THONG TIN TU FORM
export let layThongTinTuForm = () => {
  let id = document.getElementById("foodID").value;
  let name = document.getElementById("tenMon").value;
  let type = document.getElementById("loai").value;
  let price = document.getElementById("giaMon").value;
  let discount = document.getElementById("khuyenMai").value;
  let status = document.getElementById("tinhTrang").value;
  let img = document.getElementById("hinhMon").value;
  let desc = document.getElementById("moTa").value;
  // return {ma:ma, .....} ---> ES6
  return { id, name, type, price, discount, status, img, desc };
};

// SHOW DATA LEN FORM INPUT
export let showForm = (data) => {
  document.getElementById("foodID").value = data.id;
  document.getElementById("tenMon").value = data.name;
  document.getElementById("loai").value = data.type;
  document.getElementById("giaMon").value = data.price;
  document.getElementById("khuyenMai").value = data.discount;
  document.getElementById("tinhTrang").value = data.status;
  document.getElementById("hinhMon").value = data.img;
  document.getElementById("moTa").value = data.desc;
};

//RESET
// export let resetForm = () => {
//   document.getElementById("exampleModal").reset();
// };
// window.resetForm = resetForm;

// CLASS
export class Food {
  constructor(id, name, type, price, discount, status, img, desc) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.price = price;
    this.discount = discount;
    this.status = status;
    this.img = img;
    this.desc = desc;
    this.giaKhuyenMai = function () {
      return (this.price * this.discount) / 100;
    };
  }
}
