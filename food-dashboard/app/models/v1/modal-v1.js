export class MonAn {
  constructor(ten, ma, loai, khuyenMai, tinhTrang, hinhMon, moTa) {
    this.ten = ten;
    this.ma = ma;
    this.loai = loai;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhMon = hinhMon;
    this.moTa = moTa;
  }
}
